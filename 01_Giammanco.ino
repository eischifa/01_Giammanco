#include <ESP8266WiFi.h>  
#include <Wire.h>
#include <Omron_D6FPH.h>
#include <BME280I2C.h>
//**********************//
	Omron_D6FPH mySensor;
	BME280I2C bme; 
	const int zero    = D8;      
//**********************//

void setup() {
	 	Serial.begin(9600); 
		pinMode   (zero,   OUTPUT);
			digitalWrite  (zero,    HIGH);
		delay(500);
	Wire.begin();
		mySensor.begin(MODEL_0505AD3);
		bme.begin();
} 

void loop() {

	float __TEMPERATURE__(NAN), __HUMIDITY__(NAN), __PRESSURE__(NAN),__KPa__(NAN),__DIFFPRES__(NAN),__DIFFTEMP__(NAN);
		  bme.read(__PRESSURE__,
								__TEMPERATURE__,
								__HUMIDITY__
								);

__DIFFPRES__= mySensor.getPressure();
__DIFFTEMP__=mySensor.getTemperature();

/*
 String data = "{\"Temperature\":\""+String(__TEMPERATURE__)+"\",\"Humidity\":\""+String(__HUMIDITY__)+"\",\"Pressure\":\""+String(__PRESSURE__)+"\",\"DIFFPRES\":\""+String(__DIFFPRES__)+"\",\"DIFFTEMP\":\""+String(__DIFFTEMP__)+"\"}"; 
*/


 Serial.println(__DIFFPRES__);
  delay(10);
}